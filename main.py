# Move workshop map files, so they can be loaded in the game
import os
import sys
import json

WORKSHOP_FOLDER = "/run/media/ced/088e1102-b2b3-4f45-8432-536b20893101/home/ced/Games/steam2/steamapps/workshop/content/311210"
USERMAPS_FOLDER = "/run/media/ced/088e1102-b2b3-4f45-8432-536b20893101/home/ced/Games/steam2/steamapps/common/Call of Duty Black Ops III/usermaps"


def check_folders():
    return os.path.isdir(WORKSHOP_FOLDER) and os.path.isdir(USERMAPS_FOLDER)


def read_worksop_json(workshop_id):
    json_file = os.path.join(WORKSHOP_FOLDER, workshop_id, "workshop.json")
    with open(json_file) as f:
        json_content = json.load(f)
        return json_content["FolderName"]


# Creates destination folder (not the zone folder)
def create_map_destination(map_name_):
    map_folder = os.path.join(USERMAPS_FOLDER, map_name_, "zone")
    if not os.path.exists(map_folder):
        os.makedirs(map_folder)
    return map_folder


def move_map_files(workshop_id_, full_map_folder_):
    src_folder = os.path.join(WORKSHOP_FOLDER, workshop_id_)
    des_folder = full_map_folder_
    command = "mv "+src_folder+"/* '"+des_folder+"'"
    print("Command: ", command)
    ok = os.system(command)
    # cleanup original folder
    if len(os.listdir(src_folder)) == 0:
        os.removedirs(src_folder)
    return ok == 0


def dup_en_files(dest_folder_, map_name_):
    file1 = "ja_"+map_name_+".ff"
    file1_fp = os.path.join(dest_folder_, file1)
    if not os.path.exists(file1_fp):
        print(file1+" is missing, trying to copy it...")
        en_file = os.path.join(dest_folder_, file1.replace("ja_", "en_"))
        command = "cp \""+en_file+"\" \""+file1_fp+"\""
        os.system(command)

    file2 = "ja_" + map_name_ + ".xpak"
    file2_fp = os.path.join(dest_folder_, file2)
    if not os.path.exists(file2_fp):
        print(file2+" is missing, trying to copy it...")
        en_file = os.path.join(dest_folder_, file2.replace("ja_", "en_"))
        command = "cp \""+en_file+"\" \""+file2_fp+"\""
        os.system(command)


def run(workshop_id_):
    print("Map ID: ", workshop_id_)
    map_folder = os.path.join(WORKSHOP_FOLDER, map_workshop_id)
    if not os.path.exists(map_folder):
        print("Folder of the map ", map_workshop_id, " doesn't exist...")
        exit(10)
    map_name = read_worksop_json(workshop_id_)
    print("Map name: ", map_name)

    print("Make map destination folder...")
    map_destination = create_map_destination(map_name)
    if not os.path.exists(map_destination):
        print("Error, destination folder creation failed!")
        exit(11)

    print("Moving map files...")
    if not move_map_files(workshop_id_, map_destination):
        print("Error while moving files...")
        exit(12)

    # create ja files from en if needed...
    dup_en_files(map_destination, map_name)

    print("DONE")


def list_map_folders():
    folders = os.listdir(WORKSHOP_FOLDER)
    index = 1
    avail_maps = ['Quit']
    for entry in folders:
        # print(entry)  # TMP
        if not os.path.isdir(os.path.join(WORKSHOP_FOLDER, entry)):
            continue
        print(index, ":", entry + " (" + read_worksop_json(entry) + ")")
        avail_maps.append(entry)
        index += 1
    return avail_maps


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print("Checking folders exist...")
    if not check_folders():
        print("Error, at least one folder doesn't exist!")
        exit(1)
    print("OK")

    if len(sys.argv) != 2:
        print("Maps available:")
        available_folders = list_map_folders()
        selection = input("Map number or 0 to exit: ")
        if int(selection) == 0:
            exit(2)
        map_workshop_id = available_folders[int(selection)]
        print("You selected: ", map_workshop_id)
    else:
        map_workshop_id = sys.argv[1]

    if not map_workshop_id:
        print("Error, no map id provided")
        exit(3)

    run(map_workshop_id)
